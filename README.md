# README #

README for ITP 382 Mobile Gaming Final Project
Cooking Mama Space Team

To run, build to iOS, connect on the same network by choosing one device to act as a Host (Server + Client), and other device pure clients.


Currently, you NEED 2 devices to join the game, otherwise the lobby will not start. You can edit this to be Single Player in the Lobby Manager->minimumPlayers serialized variable.


Some assets we used from the asset store were PDollar, Progress Bar, and the free Network Lobby Sample by Unity.


We also took some assets from the internet to create our images, which links are listed below. 


1. http://www.spriters-resource.com/ds/cookingmama/sheet/27622/

2. http://www.spriters-resource.com/wii/cookingmamacookoff/sheet/68472/

3. http://worldartsme.com/kitchen/?order=views

4. http://www.bensound.com/royalty-free-music/track/jazzy-frenchy

5. https://www.freesound.org/people/milton./sounds/86865/

6. http://soundbible.com/tags-winning.html