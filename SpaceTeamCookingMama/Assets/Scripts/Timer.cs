﻿using ProgressBar;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
	ProgressBarBehaviour BarBehaviour;
	[SerializeField] float secondsForDish;
	private float startTime;

	void Start() {
		startTime = Time.time;
	}

	void Update() {
		if (Time.time - startTime >= secondsForDish) {
			BarBehaviour.Value = 100;
		}
		else {
			BarBehaviour = GetComponent<ProgressBarBehaviour>();
			BarBehaviour.Value = ((Time.time - startTime) / secondsForDish) * 100;
		}

		/*if ((Time.time - startTime) >= secondsForDish) {
			//Debug.Log ("Game ended");
			SceneManager.LoadScene ("_EndGame_Lose");
		} */
	}
}