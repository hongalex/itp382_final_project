﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic; 


public class SharedPanelScript : NetworkBehaviour {


	public List<GameObject> dishList = new List<GameObject>();
	private int numDishesInList = 0;

	//private float timeToGo;

	void Start() {
		//timeToGo = Time.fixedTime + 2.0f;
	}

	void Update() {
		/*if (Time.fixedTime >= timeToGo) {
			CmdUpdatePositions ();
			/timeToGo = Time.fixedTime + 2.0f;
		}*/
	}

	public void addDish(GameObject obj) {
		dishList.Add(obj);
		numDishesInList++;
	}

	public Vector3 getNextPosition() {
		switch (dishList.Count) {
			case 0:
				return gameObject.transform.position + Vector3.left * 8 + Vector3.up * 0.3f;
			case 1:
				return gameObject.transform.position + Vector3.left * 3 + Vector3.up * 0.3f;
			case 2:
				return gameObject.transform.position + Vector3.right * 2 + Vector3.up * 0.3f;

		}
		return transform.position;

	}

	//[Command]
	void CmdUpdatePositions() {
		switch (dishList.Count) {
		case 1: dishList [0].transform.position = gameObject.transform.position + Vector3.left * 8 + Vector3.up * 0.3f;
				break;
		case 2: dishList [0].transform.position = gameObject.transform.position + Vector3.left * 8 + Vector3.up * 0.3f;
				dishList [1].transform.position = gameObject.transform.position + Vector3.left * 3 + Vector3.up * 0.3f;
				break;
		case 3: dishList [0].transform.position = gameObject.transform.position + Vector3.left * 8 + Vector3.up * 0.3f;
				dishList [1].transform.position = gameObject.transform.position + Vector3.left * 3 + Vector3.up * 0.3f;
				dishList [2].transform.position = gameObject.transform.position + Vector3.right * 2 + Vector3.up * 0.3f;
				break;
		}
	}

	void OnTriggerStay (Collider other){
		if (other.tag == "Dish") {
			if ((other.gameObject.GetComponent<Dish> ().dishdown)) { 
//				other.gameObject.GetComponent<Dish>().visible = true;
//				other.gameObject.GetComponent<Dish>().reenter = false;

				if (!dishList.Contains (other.gameObject)) {
					dishList.Add (other.gameObject);
					numDishesInList++;
					CmdUpdatePositions ();
				} 
			}
		} 
	} 


	void OnTriggerExit (Collider other) {
		if (other.tag == "Dish") {
			Debug.Log("exit working");

			if(dishList.Contains(other.gameObject)) {
				dishList.Remove(other.gameObject);
				numDishesInList--;
			}
			CmdUpdatePositions ();
		}
	}
}
