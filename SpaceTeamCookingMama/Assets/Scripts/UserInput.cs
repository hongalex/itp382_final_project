﻿using UnityEngine;
using System.Collections;

using System;
using System.Collections.Generic;
using System.IO;
using PDollarGestureRecognizer;

public class UserInput : MonoBehaviour {

	//Variables for gesture
	public Transform gestureOnScreenPrefab;

	private List<Gesture> trainingSet = new List<Gesture>();

	private List<Point> points = new List<Point>();
	private int strokeId = -1;

	private Vector3 virtualKeyPosition = Vector2.zero;
	private Rect drawArea;

	private RuntimePlatform platform;
	private int vertexCount = 0;

	private List<LineRenderer> gestureLinesRenderer = new List<LineRenderer>();
	private LineRenderer currentGestureLineRenderer;

	//GUI
	private string message;

	// Use this for initialization
	void Start () {
		//Print out whether multitouch is supported
		//bool supportsMultiTouch = Input.multiTouchEnabled;
		//print("MultiTouchSupport : " + supportsMultiTouch);

		//gestureOnScreenPrefab = Transform.FindObjectOfType;

		gestureOnScreenPrefab = null;

		platform = Application.platform;
		drawArea = new Rect(0, 0, Screen.width, Screen.height);

		//Load pre-made gestures
		TextAsset[] gesturesXml = Resources.LoadAll<TextAsset>("GestureSet/10-stylus-MEDIUM/");
		foreach (TextAsset gestureXml in gesturesXml)
			trainingSet.Add(GestureIO.ReadGestureFromXML(gestureXml.text));

		//Load user custom gestures
		string[] filePaths = Directory.GetFiles(Application.persistentDataPath, "*.xml");
		foreach (string filePath in filePaths)
			trainingSet.Add(GestureIO.ReadGestureFromFile(filePath));
	}


	// Update is called once per frame
	void Update () {
		if (platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer) {
			if (Input.touchCount > 0) {
				virtualKeyPosition = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
			}
		} else {
			if (Input.GetMouseButton(0)) {
				virtualKeyPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y);
			}
		}

		//If the draw area contains the drawn stuff
		if (drawArea.Contains (virtualKeyPosition)) {
			if (Input.GetMouseButtonDown (0)) {
				//print ("Mouse button down 1");

				++strokeId;

				Transform tmpGesture = Instantiate (gestureOnScreenPrefab, transform.position, transform.rotation) as Transform;
				currentGestureLineRenderer = tmpGesture.GetComponent<LineRenderer> ();

				gestureLinesRenderer.Add (currentGestureLineRenderer);

				vertexCount = 0;
			}

			//Get every point along the stroke
			if (Input.GetMouseButton (0)) {
				//print ("Mouse button down 2");
				points.Add (new Point (virtualKeyPosition.x, -virtualKeyPosition.y, strokeId));
				if(currentGestureLineRenderer != null ) {
					currentGestureLineRenderer.SetVertexCount (++vertexCount);
					currentGestureLineRenderer.SetPosition (vertexCount - 1, Camera.main.ScreenToWorldPoint (new Vector3 (virtualKeyPosition.x, virtualKeyPosition.y, 10)));
				//print ("Adding point to virtual key position");
				}

				/*currentGestureLineRenderer.SetVertexCount (++vertexCount);
				currentGestureLineRenderer.SetPosition (vertexCount - 1, Camera.main.ScreenToWorldPoint (new Vector3 (virtualKeyPosition.x, virtualKeyPosition.y, 10)));
				//print ("Adding point to virtual key position");*/
			}

			if (Input.GetMouseButtonUp (0)) {
				//print ("Mouse button up");
				Gesture candidate = new Gesture (points.ToArray ());
				Result gestureResult = PointCloudRecognizer.Classify (candidate, trainingSet.ToArray ());

				//print ("Raycasting");
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if(Physics.Raycast(ray, out hit))
				{
					//print ("In raycasting block");

					if (hit.collider.gameObject.tag == "Panel" && hit.collider.gameObject.GetComponent<Space1>().isCorrectDish) {
						//print ("Hit a panel!");
						message = gestureResult.GestureClass + " " + gestureResult.Score;
						print ("Message: " + message);

						switch (hit.collider.gameObject.GetComponent<Space1> ().check) {
						case 1:
							//textToDisplay = "Fry Ingredients";
							checkVertical(gestureResult);

							break;
						case 2:	
							checkVertical (gestureResult);
							//textToDisplay = "Cook Rice";
							break;
						case 3:
							checkCircle (gestureResult);
							//textToDisplay = "Stir Pot";
							break;
						case 4:
							checkHorizontal (gestureResult);
							//textToDisplay = "Crack Egg";
							break;
						case 5:
							checkHorizontal (gestureResult);
							//textToDisplay = "Add Sauce";
							break;
						case 6:
							checkCircle (gestureResult);
							//textToDisplay = "Wash Dishes";
							break;
						case 7:
							checkVertical (gestureResult);
							//textToDisplay = "Cut Ingredients";
							break;
						case 8:
							checkHorizontal (gestureResult);
							//textToDisplay = "Plate Food";
							break;
					}
				}


				strokeId = -1;

				points.Clear ();

				foreach (LineRenderer lineRenderer in gestureLinesRenderer) {

					lineRenderer.SetVertexCount (0);
					Destroy (lineRenderer.gameObject);
				}

				gestureLinesRenderer.Clear ();
			}
		}
	}
	}

	void checkCircle(Result gestureResult) {
		if (gestureResult.GestureClass.Equals ("D")) {
			print ("Circle");
			//ProgressCircle script = hit.collider.gameObject.GetComponent<ProgressCircle> (); 
			//script.fillAmount (); 
		}
	}
	void checkHorizontal(Result gestureResult) {
		if(gestureResult.GestureClass.Equals ("line")) {
			print("Horizontal line");
			//ProgressCircle script = hit.collider.gameObject.GetComponent<ProgressCircle> (); 
			//script.fillAmount (); 
		}
	}
	void checkVertical(Result gestureResult) {
		if (gestureResult.GestureClass.Equals ("pitchfork") || gestureResult.GestureClass.Equals ("exclamation point")) {
			print ("Vertical line");
			//ProgressCircle script = hit.collider.gameObject.GetComponent<ProgressCircle> (); 
			//script.fillAmount (); 
		}
	}

	void OnGUI() {
		GUI.Label (new Rect (10, Screen.height - 40, 500, 50), message);
	}

}

/*
using UnityEngine;
using System.Collections;

using System;
using System.Collections.Generic;
using System.IO;
using PDollarGestureRecognizer;

public class UserInput : MonoBehaviour {

	//Variables for gesture
	public Transform gestureOnScreenPrefab;

	private List<Gesture> trainingSet = new List<Gesture>();

	private List<Point> points = new List<Point>();
	private int strokeId = -1;

	private Vector3 virtualKeyPosition = Vector2.zero;
	private Rect drawArea;

	private RuntimePlatform platform;
	private int vertexCount = 0;

	private List<LineRenderer> gestureLinesRenderer = new List<LineRenderer>();
	private LineRenderer currentGestureLineRenderer;

	//GUI
	private string message;

	// Use this for initialization
	void Start () {
		//Print out whether multitouch is supported
		bool supportsMultiTouch = Input.multiTouchEnabled;
		print("MultiTouchSupport : " + supportsMultiTouch);


		platform = Application.platform;
		drawArea = new Rect(0, 0, Screen.width, Screen.height);

		//Load pre-made gestures
		TextAsset[] gesturesXml = Resources.LoadAll<TextAsset>("GestureSet/10-stylus-MEDIUM/");
		foreach (TextAsset gestureXml in gesturesXml)
			trainingSet.Add(GestureIO.ReadGestureFromXML(gestureXml.text));

		//Load user custom gestures
		string[] filePaths = Directory.GetFiles(Application.persistentDataPath, "*.xml");
		foreach (string filePath in filePaths)
			trainingSet.Add(GestureIO.ReadGestureFromFile(filePath));
	}


	// Update is called once per frame
	void Update () {
		if (platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer) {
			if (Input.touchCount > 0) {
				virtualKeyPosition = new Vector3 (Input.GetTouch (0).position.x, Input.GetTouch (0).position.y);
			}
		} else {
			if (Input.GetMouseButton (0)) {
				virtualKeyPosition = new Vector3 (Input.mousePosition.x, Input.mousePosition.y);
			}
		}

		//If the draw area contains the drawn stuff
		if (drawArea.Contains (virtualKeyPosition)) {
			if (Input.GetMouseButtonDown (0)) {
				//print ("Mouse button down 1");

				++strokeId;

				Transform tmpGesture = Instantiate (gestureOnScreenPrefab, transform.position, transform.rotation) as Transform;
				currentGestureLineRenderer = tmpGesture.GetComponent<LineRenderer> ();

				gestureLinesRenderer.Add (currentGestureLineRenderer);

				vertexCount = 0;
			}

			//Get every point along the stroke
			if (Input.GetMouseButton (0)) {
				//print ("Mouse button down 2");
				points.Add (new Point (virtualKeyPosition.x, -virtualKeyPosition.y, strokeId));

				currentGestureLineRenderer.SetVertexCount (++vertexCount);
				currentGestureLineRenderer.SetPosition (vertexCount - 1, Camera.main.ScreenToWorldPoint (new Vector3 (virtualKeyPosition.x, virtualKeyPosition.y, 10)));
				//print ("Adding point to virtual key position");
			}

			if (Input.GetMouseButtonUp (0)) {
				print ("Mouse button up");
				Gesture candidate = new Gesture (points.ToArray ());
				Result gestureResult = PointCloudRecognizer.Classify (candidate, trainingSet.ToArray ());

				print ("Raycasting");
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast (ray, out hit)) {
					print ("In raycasting block");
					if (hit.collider.gameObject.tag == "Panel" && hit.collider.gameObject.GetComponent<Space1> ().isCorrectDish) {
						print ("Hit a panel!");
						message = gestureResult.GestureClass + " " + gestureResult.Score;
						print ("Message: " + message);
						if (gestureResult.GestureClass.Equals ("D")) {
							print ("Circle");
						} else if (gestureResult.GestureClass.Equals ("pitchfork") || gestureResult.GestureClass.Equals ("exclamation point")) {
							print ("Vertical line");
						} else if (gestureResult.GestureClass.Equals ("line")) {
							print ("Horizontal line");
						}
					}
				}
			}
		}
	}
	/*

		
						
		//Mouse input
		//if (Input.GetMouseButton (0)) {
			//Raycast the mouse position
			Ray screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (screenRay, out hit)) {
				//If pressed a panel
				if (hit.collider.gameObject.tag == "Panel" && hit.collider.gameObject.GetComponent<Space1>().isCorrectDish) {
					print ("Using mouse");
					print ("Tapped a " + hit.collider.gameObject.name);
					//Get the progress circle and fill amount
					ProgressCircle script = hit.collider.gameObject.GetComponent<ProgressCircle> (); 
					script.fillAmount (); 
				} 
			} 
		}

		//Swipe input
		if (Input.touchCount > 0) {
			//print ("touchcount > 0");
			//print("in swipe");
			Ray screenRay = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
			RaycastHit hit;
			if (Physics.Raycast (screenRay, out hit)) {
				//If pressed a panel
				//print("raycastin");
				if (hit.collider.gameObject.tag == "Panel" && hit.collider.gameObject.GetComponent<Space1>().isCorrectDish) {
					//print ("a panel!");
					foreach (Touch touch in Input.touches) {
						switch (touch.phase) {
						case TouchPhase.Began:
							//this is a new touch 
							isSwipe = true;
							fingerStartTime = Time.time;
							fingerStartPos = touch.position;
							break;

						case TouchPhase.Canceled:
							// The touch is being canceled 
							isSwipe = false;
							break;

						case TouchPhase.Ended:

							float gestureTime = Time.time - fingerStartTime;

							//touch.position is starting point
							float gestureDist = (touch.position - fingerStartPos).magnitude;

							if (isSwipe && gestureTime < maxSwipeTime && gestureDist > minSwipeDist) {
								Vector2 direction = touch.position - fingerStartPos;
								Vector2 swipeType = Vector2.zero;

								if (Mathf.Abs (direction.x) > Mathf.Abs (direction.y)) {
									// the swipe is horizontal:
									swipeType = Vector2.right * Mathf.Sign (direction.x);
								} else {
									// the swipe is vertical:
									swipeType = Vector2.up * Mathf.Sign (direction.y);
								}

								if (swipeType.x != 0.0f) {
									if (swipeType.x > 0.0f) {
										//print ("Right " + hit.collider.gameObject.name);
										ProgressCircle script = hit.collider.gameObject.GetComponent<ProgressCircle> (); 
										script.fillAmount (); 
									} else {
										// MOVE LEFT
										//print ("Left " + hit.collider.gameObject.name);
										ProgressCircle script = hit.collider.gameObject.GetComponent<ProgressCircle> (); 
										script.fillAmount (); 
									}
								}

								if (swipeType.y != 0.0f) {
									if (swipeType.y > 0.0f) {
										// MOVE UP
										//print ("Up " + hit.collider.gameObject.name);
										ProgressCircle script = hit.collider.gameObject.GetComponent<ProgressCircle> (); 
										script.fillAmount (); 
									} else {
										// MOVE DOWN
										//print ("Down " + hit.collider.gameObject.name);
										ProgressCircle script = hit.collider.gameObject.GetComponent<ProgressCircle> (); 
										script.fillAmount (); 
									}
								}

							}

							break;
						}
					}
				}
					

				strokeId = -1;

				points.Clear ();

				foreach (LineRenderer lineRenderer in gestureLinesRenderer) {

					lineRenderer.SetVertexCount (0);
					Destroy (lineRenderer.gameObject);
				}

				gestureLinesRenderer.Clear ();
			}
		}
	}


	void OnGUI() {
		GUI.Label (new Rect (10, Screen.height - 40, 500, 50), message);

	}

}
*/
