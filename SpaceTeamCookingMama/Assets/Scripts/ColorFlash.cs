﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColorFlash : MonoBehaviour {
	public int flashSpeed;
	private Text text;
	private Color[] colors = {Color.red, Color.blue, Color.cyan, Color.yellow, Color.green,
		Color.magenta, Color.white}; 
	int count = 0;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (count == flashSpeed) {
			int index = Random.Range (0, colors.Length);

			text.color = colors [index];
			count = 0;
		} else {
			count++;
		}

	}
}
