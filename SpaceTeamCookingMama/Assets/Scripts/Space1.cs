﻿using UnityEngine;
using System.Collections;

public class Space1 : MonoBehaviour {

	public int check; //THIS IS THE NUMBER ASSOCIATED WITH THIS PANEL 
	public bool alreadyChecked = false; 
	public bool isCorrectDish = false;

	void OnTriggerStay (Collider other){
		if (other.tag == "Dish"){
			if ((other.gameObject.GetComponent<Dish> ().dishdown) && (!alreadyChecked)) { //drop the dish and first time you are checking to see if its the correct dish 
				Dish script = other.gameObject.GetComponent<Dish> (); 
				if (script.checkQueue () == check) {
					//correct dish 
					//Debug.Log ("Correct Dish!"); 
					isCorrectDish = true;
					script.popQueue (); 
					//snap to middle of panel 
					other.gameObject.transform.position = gameObject.transform.position; 
					//disable until completed 
					//other.enabled = !other.enabled;
					alreadyChecked = true; 

					//Change text for the object to be something else
					TextMesh textMesh = other.gameObject.GetComponentInChildren<TextMesh> ();
                    print(textMesh.text); 
					string textToDisplay;
					switch (check) {
						case 1:
							textToDisplay = "Fry Ingredients";
							break;
						case 2:	
							textToDisplay = "Cook Rice";
							break;
						case 3:
							textToDisplay = "Stir Pot";
							break;
                        case 4:
                            textToDisplay = "Crack Egg";
                            break;
                        case 5:
                            textToDisplay = "Add Sauce";
                            break;
                        case 6:
                            textToDisplay = "Wash Dishes";
                            break;
                        case 7:
                            textToDisplay = "Cut Ingredients";
                            break;
                        case 8:
                            textToDisplay = "Plate Food";
                            break;
                        default:
							textToDisplay = "";
							break;
					}
						
					textMesh.text = textToDisplay;



				} else {
					//Debug.Log ("Wrong Dish!"); 
					isCorrectDish = false;
				} 
			}
		} 
	} 
}
