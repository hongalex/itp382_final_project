﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;
using UnityEngine.SceneManagement;
using System.Collections.Generic; 
using System.Collections;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Xml;

namespace UnityStandardAssets.Network{
public class MainGameManager : MonoBehaviour {

	public List<GameObject> panelList; 
	public List<Vector3> panelPosition; 
	public GameObject curryPrefab;
	public GameObject omelettePrefab;
	public GameObject hamburgerPrefab;
    XmlDocument cookbook;
    //public GameObject prefab;
    public List<GameObject> dishList; 
	public int numofDishes; 

    private bool gameIsRunning;

	public GameObject localPlayer;

	public GameObject selectedObject;
	public bool hasObject;

	private int index;

	private float endTime;

	// Use this for initialization
	void Start () {

		//get playerconnections from lobbymanager 
		//List<NetworkConnection> connectionList = LobbyManager.s_Singleton.connections;
		//NetworkConnection currConnection = LobbyManager.s_Singleton.client.connection;
		//index = connectionList.IndexOf(currConnection);


        dishList = new List<GameObject>(); 


        //create the prefabs
        dishList.Add(Instantiate(curryPrefab));
        dishList.Add(Instantiate(omelettePrefab));
        dishList.Add(Instantiate(hamburgerPrefab));

		numofDishes = dishList.Count; 

        /*if (NetworkServer.active) {

			NetworkServer.Spawn (curry);
			NetworkServer.Spawn (omelette);
			NetworkServer.Spawn (hamburger);
		}*/

			/*
        //READ XML to enqueue dishes 
        StreamReader reader = new StreamReader(Application.dataPath + "/xmlDocs/Cookbook.xml");
        string contents = reader.ReadToEnd();
        reader.Close();

        cookbook = new XmlDocument();
        cookbook.LoadXml(contents.ToString());
        parseDishes();
			*/
		gameIsRunning = true;
		hasObject = false;

	}

		public void createPanels(List<int> values) {
		//TODO: RECIEVE RANDOMIZED INT LIST FROM SERVERE 
		//PLACE PANELS BASED ON NUMBER ORDER 
		//1 2
		//3 4 

		//Find Local Player
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("Player")) {
			if(obj.GetComponent<NetworkIdentity>().isLocalPlayer) {
				localPlayer = obj; 
			}
		}
		List<int> testing = values;


				
        //instantiate panel prefabs to hard coded panel positions 
        GameObject panel1 = Resources.Load("Panels/Panel " + testing[0]) as GameObject;
        Instantiate(panel1, new Vector3(-6.14f, 2.67f, 3.6f), new Quaternion(0, 0, 180, 0)); //top left
        GameObject panel2 = Resources.Load("Panels/Panel " + testing[1]) as GameObject;
        Instantiate(panel2, new Vector3(4.45595f, 2.550858f, 3.6f), new Quaternion(0, 0, 180, 0)); //top right 
        GameObject panel3 = Resources.Load("Panels/Panel " + testing[2]) as GameObject;
        Instantiate(panel3, new Vector3(4.45595f, -2.503218f, 3.6f), new Quaternion(0, 0, 180, 0)); //bottom right
        GameObject panel4 = Resources.Load("Panels/Panel " + testing[3]) as GameObject;
        Instantiate(panel4, new Vector3(-5.723744f, -2.503218f, 3.6f), new Quaternion(0, 0, 180, 0)); //bottom left */
			
	}
			

	public static void correctPanel(){
		print("correct Panel!"); 
	}

    void parseDishes()
    {
        XmlNodeList dishNodes = cookbook.GetElementsByTagName("Dish");
        float offset = -10.49f;
        int i = 0; //this goes through the list of dishes 
        foreach (XmlNode dishNode in dishNodes)
        {
            //assign dish info for a prefab dish 
            //and spawn it 
            offset += 4;
            //Vector3 spawnLoc = new Vector3(offset, 6.07f, 3.6f);
            //GameObject dish = (GameObject)Instantiate(prefab, spawnLoc, Quaternion.identity);
            //Add dish name to dish prefab 
            dishList[i].GetComponent<Dish>().dishName = dishNode.InnerText;

            i++; 
        }


        //Add panel number queue to dish prefab 
        //string recipeQ = dishNode.SelectSingleNode("Queue").InnerText;
        XmlNodeList recipeQ = cookbook.GetElementsByTagName("Queue");
        //print(recipeQ.Count); 
        int j = 0; 
        foreach (XmlNode recipe in recipeQ)
        {
            string recipeString = recipe.InnerText;
            char[] delimiterChars = { ' ', '[', ']', ',' };
            string[] recipeNums = recipeString.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
            foreach (string number in recipeNums)
            {
                int test = Int32.Parse(number);
					print(test);
                dishList[j].GetComponent<Dish>().panelQueue.Enqueue(test);
            }
            j++; 
        }

    }


    // Update is called once per frame
    void Update () {
		

		if (numofDishes == 0) { //WIN STATE 
                if (gameIsRunning) {
                    endTime = Time.time;
                    gameIsRunning = false;
                } else {
                    if (Time.time >= endTime + 1.0f) {
                        SceneManager.LoadScene ("_EndGame_Win");
                    }
                }

		}




	
	}
}
}