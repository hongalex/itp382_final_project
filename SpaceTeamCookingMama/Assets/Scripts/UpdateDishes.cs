﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;


public class UpdateDishes : NetworkBehaviour {

	private Vector3 curryPos;
	private Vector3 omelettePos;
	private Vector3 hamburgerPos;

	private List<GameObject> dishes;


	private float timeToGo;

	void Start() {
		dishes = new List<GameObject> ();
		dishes.Add (GameObject.Find ("Curry(Clone)"));
		dishes.Add (GameObject.Find ("Omelette(Clone)"));
		dishes.Add (GameObject.Find ("Hamburger(Clone)"));

		curryPos = dishes[0].transform.position;
		omelettePos = dishes[1].transform.position;
		hamburgerPos = dishes[2].transform.position;

		timeToGo = Time.fixedTime + 1.0f;
	}

	void FixedUpdate() {
		if (Time.fixedTime >= timeToGo) {
			if (isLocalPlayer) {
				TransmitPositions();
				//LerpPosition ();
				timeToGo = Time.fixedTime + 0.50f;
			}



		}
	}

	/*void LerpPosition() {
		if (!isLocalPlayer) {
			//Debug.Log ("receiving new positions");
			curryTransform.position = curryPos; //Vector3.Lerp (curryTransform.position, curryPos, Time.deltaTime * lerpRate);
			omeletteTransform.position = omelettePos; //Vector3.Lerp (omeletteTransform.position, omelettePos, Time.deltaTime * lerpRate);
			hamburgerTransform.position = hamburgerPos; //Vector3.Lerp (hamburgerTransform.position, hamburgerPos, Time.deltaTime * lerpRate);

		}
	}*/
	
	[Command]	
	void CmdProvidePositionToServer(Vector3 pos1, Vector3 pos2, Vector3 pos3) {
		if (pos1 != Vector3.zero) {
			curryPos = pos1;
			RpcMoveCurry (curryPos);
		}
		if (pos2 != Vector3.zero) {
			omelettePos = pos2;
			RpcMoveOmelette (omelettePos);
		}
		if (pos3 != Vector3.zero) {

			hamburgerPos = pos3;
			RpcMoveBurger (hamburgerPos);
		}
	}

	[Command]
	void CmdDisable(int id) {
		RpcDisable (id);
	}

	[Command]
	void CmdEnable(int id) {
		RpcEnable(id);
	}

	[Command]
	void CmdPopQueue(int id) {
		RpcPopQueue(id);
	}

	[ClientRpc]
	void RpcPopQueue(int id) {
		if (!dishes[id].GetComponent<Dish>().poppedQueue) {
			dishes[id].GetComponent<Dish>().popQueue();
		}
		dishes[id].GetComponent<Dish>().poppedQueue = false;
	}

	[ClientRpc]
	void RpcDisable(int id) {
		if (dishes[id].GetComponent<Dish> ().visible) {
			dishes[id].GetComponent<Renderer> ().enabled = false;
			dishes[id].transform.GetChild (0).GetComponent<Renderer> ().enabled = false;
			dishes[id].GetComponent<Collider> ().enabled = false;
		} else {
			dishes[id].GetComponent<Dish> ().visible = true;
		}
	}

	[ClientRpc]
	void RpcEnable(int id) {
		dishes[id].GetComponent<Renderer> ().enabled = true;
		dishes[id].GetComponent<Collider> ().enabled = true;

		dishes[id].GetComponent<Dish> ().reenter = false;
	}

	[ClientRpc]
	void RpcMoveCurry(Vector3 pos) {
		GameObject curry = GameObject.Find ("Curry(Clone)");

		if(curry.transform.position!=pos) curry.transform.position = pos;
	}

	[ClientRpc]
	void RpcMoveOmelette(Vector3 pos) {
		GameObject omelette = GameObject.Find ("Omelette(Clone)");

		if(omelette.transform.position!=pos) omelette.transform.position = pos;

	}	

	[ClientRpc]
	void RpcMoveBurger(Vector3 pos) {
		GameObject hamburger = GameObject.Find ("Hamburger(Clone)");

		if(hamburger.transform.position!=pos) hamburger.transform.position = pos;

	}

	[ClientCallback]
	void TransmitPositions() {
		if (isLocalPlayer) {
			//Debug.Log ("transmitting positions");
			//curry = GameObject.Find ("Curry(Clone)");
			//omelette = GameObject.Find ("Omelette(Clone)");
			//hamburger = GameObject.Find ("Hamburger(Clone)");


			Vector3 pos1, pos2, pos3;
			if (dishes[0].transform.position == curryPos) {
				pos1 = Vector3.zero;
			} else {
				pos1 = dishes[0].transform.position;
				curryPos = pos1;
			}

			if (dishes[1].transform.position == omelettePos) {
				pos2 = Vector3.zero;
			} else {
				pos2 = dishes[1].transform.position;
				omelettePos = pos2;
			}
			
			if (dishes[2].transform.position == hamburgerPos) {
				pos3 = Vector3.zero;
			} else {
				pos3 = dishes[2].transform.position;
				hamburgerPos = pos3;
			}
			
			CmdProvidePositionToServer (pos1, pos2, pos3);


			for (int i = 0; i < dishes.Count; i++) {
				if (!dishes[i].GetComponent<Dish> ().visible) {
					CmdDisable (i);
				} else if (dishes[i].GetComponent<Dish> ().reenter) {
					CmdEnable (i);
				}

				if (dishes[i].GetComponent<Dish>().poppedQueue) {
					CmdPopQueue(i);
				}
			}

		}
	}
}
