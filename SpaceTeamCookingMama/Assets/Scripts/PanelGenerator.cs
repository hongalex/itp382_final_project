﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;


public class PanelGenerator : NetworkBehaviour {

	//Only created on server
	public List<int> panels;
	public int[] numbers;

	private bool createdPanels;

	private float timeToGo;

	void Start () {
		createdPanels = false;
		panels = new List<int>();
		numbers = new int[10];
		timeToGo = Time.fixedTime + 0.5f;

	}

	[ClientCallback]
	void FixedUpdate() {
		if (isLocalPlayer & isServer)
		{
			if (Time.fixedTime >= timeToGo)
			{
				if (!createdPanels)
				{
					CmdGeneratePanels();
					createdPanels = true;
				}

			}
		}
	}

	[Command]
	void CmdGeneratePanels() {
		while (panels.Count < 8)
		{
			int number = Random.Range(1, 9);
			if (!panels.Contains(number))
			{
				panels.Add(number);
			}
		}
		createdPanels = true;
		for (int i = 0; i < panels.Count; i++)
		{
			numbers[i] = panels[i];
		}
		RpcSendPanels(numbers);		
	}

	[ClientRpc]
	void RpcSendPanels(int[] array) {
		//Debug.Log("created client");
		this.numbers = array;
		List<int> values = new List<int>();
		if (isServer)
		{
			for (int i = 0; i < 4; i++)
			{
				values.Add(numbers[i]);
			}
		} else {
			for (int i = 4; i < 8; i++) {
				values.Add(numbers[i]);
			}
		}

		Camera.main.gameObject.GetComponent<UnityStandardAssets.Network.MainGameManager>().createPanels(values);

	}

}
