﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement; 
using System.Collections.Generic; 
using System.Collections;


public class Dish : NetworkBehaviour {

	public int dishtype;

	
	public Queue<int> panelQueue;
    public string dishName; 
	public bool dishdown = true; 
	//GameObject panelIn; 
	private Vector3 screenPoint;
	private Vector3 offset;

	private UnityStandardAssets.Network.MainGameManager mgm;

	public bool visible;
	public bool reenter;
	private bool done; 

	public bool poppedQueue;
	void Awake () {
		panelQueue = new Queue<int>(); 

	}

	void Start() {
		mgm = Camera.main.gameObject.GetComponent<UnityStandardAssets.Network.MainGameManager> ();
		visible = true;
		reenter = false;
		poppedQueue = false;
		done = false;

		switch (dishtype)
		{
			case 1: //curry 71325
				panelQueue.Enqueue(7); 
				panelQueue.Enqueue(1); 
				panelQueue.Enqueue(3); 
				panelQueue.Enqueue(2); 
				panelQueue.Enqueue(5); 
				break;
			case 2: //omelet 74186 

				panelQueue.Enqueue(4); 
				panelQueue.Enqueue(7); 
				panelQueue.Enqueue(1); 
				panelQueue.Enqueue(8); 
				panelQueue.Enqueue(5);
				break;
			case 3: //hamburger 17485
				panelQueue.Enqueue(7); 
				panelQueue.Enqueue(4); 
				panelQueue.Enqueue(1); 
				panelQueue.Enqueue(8); 
				panelQueue.Enqueue(6); 
				break;
			default:
				break;
		}
				
		//print(gameObject.name + " " + panelQueue.Peek());

	}

	
	void OnTriggerEnter (Collider col){
		if(col.tag == "Panel"){// if its a panel 
			//get panel 
			//panelIn = col.gameObject; 
		} 
	} 

	// Update is called once per frame
	void Update () {
		if (panelQueue.Count == 0 && !done) { //task are done! 
			//print("done with dish");
			mgm.numofDishes--;
            visible = false;
            gameObject.GetComponent<Renderer>().enabled = false;
            gameObject.GetComponent<Collider>().enabled = false;


			done = true;
			//WIN STATE 
			//Debug.Log("You Win!"); 
			//SceneManager.LoadScene ("_EndGame_Win"); 
		} 
	}

	public int checkQueue(){
		if (panelQueue.Count != 0) {
			return panelQueue.Peek (); 
		} else {
			return -1; 
		}
	} 

	public void popQueue(){
		poppedQueue = true;
		panelQueue.Dequeue (); 
	} 
		

	void OnMouseDown(){
		mgm.selectedObject = gameObject;
		mgm.hasObject = true;

//		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
//		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
//		CmdMouseUp (false);

	}

	/*void OnMouseUp(){
		CmdMouseUp (true);

	} */

	/*void OnMouseDrag(){
		Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
		Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
		CmdMouseDrag (cursorPosition);

	}*/

	//[Command]
	/*void CmdMouseDrag(Vector3 newPosition) {
		transform.position = newPosition;
	}

	//[Command]
	void CmdMouseUp(bool up) {
		dishdown = up;
	}*/
}
