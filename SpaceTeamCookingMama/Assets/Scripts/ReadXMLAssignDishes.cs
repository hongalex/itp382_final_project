﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using System.IO;
using System;
using System.Linq; 
using System.Text;
using System.Xml; 

public class ReadXMLAssignDishes : MonoBehaviour {

    XmlDocument cookbook;
    public GameObject prefab; 

	// Use this for initialization
	void Start () {
        StreamReader reader = new StreamReader(Application.dataPath + "/xmlDocs/Cookbook.xml");
        string contents = reader.ReadToEnd();
        reader.Close();

        cookbook = new XmlDocument();
        cookbook.LoadXml(contents.ToString());
        parseDishes(); 

	}

    void parseDishes()
    {
        XmlNodeList dishNodes = cookbook.GetElementsByTagName("Dish");
        float offset = -10.49f; 
        foreach(XmlNode dishNode in dishNodes)
        {
            //assign dish info for a prefab dish 
            //and spawn it 
            offset += 4; 
            Vector3 spawnLoc = new Vector3(offset, 6.07f, 3.6f);
            GameObject dish = (GameObject)Instantiate(prefab, spawnLoc, Quaternion.identity);
            //Add dish name to dish prefab 
            dish.GetComponent<Dish>().dishName = dishNode.InnerText; 

            //Add panel number queue to dish prefab 
            string recipeQ = dishNode.SelectSingleNode("Queue").InnerText;
            char[] delimiterChars = {' ', '[', ']', ','};
            string[] recipeNums = recipeQ.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
            foreach(string number in recipeNums)
            {
                int test = Int32.Parse(number); 
                dish.GetComponent<Dish>().panelQueue.Enqueue(test); 
            }
        }

    }
	
	// Update is called once per frame
	void Update () {
	    
	}
}
