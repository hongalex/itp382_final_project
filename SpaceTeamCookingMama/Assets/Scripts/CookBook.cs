﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using System.Text; 
using System.Collections.Generic;


//using UnityEditor; 

public class CookBook : MonoBehaviour{
    //GENERATES ALL RECIPES INTO AN XML FILE 

    //List<int> recipes = new List<int>(); 
    private string path; 

    void Awake()
    {
        GenerateCookBook(); 

        //XmlElement recipes = (XmlElement)book.AppendChild(book.CreateElement("recipes")); 
    }

    void GenerateCookBook()
    {

        XmlDocument book = new XmlDocument();
        path = Application.dataPath + "/xmlDocs/Cookbook.xml"; 
        //book.Load(Application.dataPath + "/xmlDocs/Cookbook.xml");

        XmlElement root = book.CreateElement("Data");
        book.AppendChild(root);

        //ADD the recipes element 
        XmlElement recipesElement = book.CreateElement("numRecipes");
        recipesElement.InnerText = "3"; //todo: provides number of recipes 
        root.AppendChild(recipesElement);


        XmlElement recipeElement = book.CreateElement("Recipes");
        root.AppendChild(recipeElement);
        //HAMBURGER
        XmlElement recipeName = book.CreateElement("Dish");
        recipeName.InnerText = "Hamburger"; 
        XmlElement recipeQueue = book.CreateElement("Queue"); 
        recipeQueue.InnerText = "[7, 1, 3, 2, 5]";
        recipeElement.AppendChild(recipeName);
        recipeElement.AppendChild(recipeQueue);

        //OMELET
        XmlElement recipeName1 = book.CreateElement("Dish");
        recipeName1.InnerText = "Omelet";
        XmlElement recipeQueue1 = book.CreateElement("Queue");
        recipeQueue1.InnerText = "[7, 4, 1, 8, 6]";
        recipeElement.AppendChild(recipeName1);
        recipeElement.AppendChild(recipeQueue1);

        //CURRY 
        XmlElement recipeName3 = book.CreateElement("Dish");
        recipeName3.InnerText = "Curry";
        XmlElement recipeQueue3 = book.CreateElement("Queue");
        recipeQueue3.InnerText = "[7, 1, 4, 8, 5]";
        recipeElement.AppendChild(recipeName3);
        recipeElement.AppendChild(recipeQueue3);

        StreamWriter outStream = File.CreateText(path);
        book.Save(outStream);
        outStream.Close(); 
        
    }
   

    


}
