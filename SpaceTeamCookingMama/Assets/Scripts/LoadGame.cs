﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadGame : MonoBehaviour {

	void DeactivateChildren(GameObject g, bool a) {
		g.SetActive (a); 

		foreach (Transform child in g.transform) {
			DeactivateChildren(child.gameObject, a);
		}
	}

	public void LoadScene() {
		GameObject p = gameObject.transform.parent.gameObject;
		p.SetActive (false);
		
		foreach (Transform child in p.transform) {
			DeactivateChildren(child.gameObject, false);
		}
		SceneManager.LoadScene( "ProgressScene" );
	}
}
