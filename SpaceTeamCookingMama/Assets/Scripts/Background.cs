﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Camera cam = Camera.main;

	    float pos = (cam.nearClipPlane + 10.0f);

	    transform.position = cam.transform.position + cam.transform.forward * pos;
	    transform.LookAt (cam.transform);
	    transform.Rotate (90.0f, 0.0f, 0.0f);

	    float h = (Mathf.Tan(cam.fieldOfView*Mathf.Deg2Rad*0.5f)*pos*2f) /10.0f;

	    transform.localScale = new Vector3(h*cam.aspect,1f, h);
	}

	// Update is called once per frame
	void Update () {

	/*
		Camera cam = Camera.main; 

		float position = (cam.nearClipPlane + 0.01f); 
		transform.position = cam.transform.position + cam.transform.forward * position; 

		float height = Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad*0.5f) * position * 2f; 
		transform.localScale = new Vector3(height * cam.aspect, height * cam.aspect, 1f); 
		*/
	}
}
