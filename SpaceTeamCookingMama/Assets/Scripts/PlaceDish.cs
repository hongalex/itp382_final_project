﻿using UnityEngine;
using System.Collections;

public class PlaceDish : MonoBehaviour {
	private UnityStandardAssets.Network.MainGameManager mgm;
	public int check; 

	void Start() {
		mgm = Camera.main.gameObject.GetComponent<UnityStandardAssets.Network.MainGameManager> ();
	}

	void OnMouseDown() {
		if (mgm.hasObject) {
			if ((gameObject.tag == "Panel")&&(mgm.selectedObject.GetComponent<Dish>().checkQueue() == check)) {
				mgm.selectedObject.GetComponent<Dish> ().visible = false;
				mgm.selectedObject.GetComponent<Dish> ().reenter = false;
				mgm.selectedObject.transform.GetChild (0).GetComponent<Renderer> ().enabled = true;

				mgm.selectedObject.transform.position = gameObject.transform.position;

				Dish script = mgm.selectedObject.GetComponent<Dish> (); 
				script.popQueue (); 

				TextMesh textMesh = mgm.selectedObject.GetComponentInChildren<TextMesh> ();
				//print(textMesh.text); 
				string textToDisplay;
				switch (script.checkQueue()) {
				case 1:
					textToDisplay = "Saute ingredients";
					break;
				case 2:	
					textToDisplay = "Cook Rice";
					break;
				case 3:
					textToDisplay = "Stir Pot";
					break;
				case 4:
					textToDisplay = "Crack Egg";
					break;
				case 5:
					textToDisplay = "Add Sauce";
					break;
				case 6:
					textToDisplay = "Wash Dishes";
					break;
				case 7:
					textToDisplay = "Cut Ingredients";
					break;
				case 8:
					textToDisplay = "Plate Food";
					break;
				default:
					textToDisplay = "";
					break;
				}

				textMesh.text = textToDisplay;
				mgm.hasObject = false;
				mgm.selectedObject = null;



			} else if (gameObject.tag == "MainPanel") {
				mgm.selectedObject.GetComponent<Dish> ().visible = true;
				mgm.selectedObject.GetComponent<Dish> ().reenter = true;
				mgm.selectedObject.transform.position = gameObject.GetComponent<SharedPanelScript> ().getNextPosition ();
				//gameObject.GetComponent<SharedPanelScript>().addDish(mgm.selectedObject);
				mgm.hasObject = false;
				mgm.selectedObject = null;

			}
		}
	}
}
