﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Direction : MonoBehaviour {

	public GameObject panel;
	public Image image;
	public bool taskcompletebool = false; 

	// Use this for initialization
	void Start () {
		image = panel.GetComponent<Image> ();
	}

	// Update is called once per frame
	void Update () {
		if (image.fillAmount == 1f) {
			image.fillAmount = 0f;
		} else {
			fillAmount ();
		}

	}

	public void fillAmount(){
		image.fillAmount += 0.07f;
	}


}


