﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProgressCircle : MonoBehaviour {

	public GameObject panel;
	public Image image;
	public bool taskcompletebool = false; 

	// Use this for initialization
	void Start () {
		image = panel.GetComponent<Image> ();
	}

	// Update is called once per frame
	void Update () {
	/*
		if (Input.GetMouseButton (0)) {
			image.fillAmount += 0.01f;
		} else if (Input.GetMouseButton (1)) {
			image.fillAmount -= 0.01f;
		}*/

	}

	public void fillAmount(){
		image.fillAmount += 0.07f;
		if (image.fillAmount == 1f) {
			taskCompleted ();
		} 

	} 

	//ONCE COMPLETELY FILLED, ENABLE CHECKMARK 
	public void taskCompleted(){
		image.color = new Color (0, 1, 0, 1); 

		//Change text for the object to be something else
		/*TextMesh textMesh = other.gameObject.GetComponentInChildren<TextMesh> ();
		string textToDisplay;
		switch (check) {
		case 1:
			textToDisplay = "Prepare rice!";
			break;
		case 2:	
			textToDisplay = "Stir pot of curry!";
			break;
		case 3:
			textToDisplay = "Sauté ingredients!";
			break;
		default:
			textToDisplay = "";
			break;
		}

		textMesh.text = textToDisplay;*/

		correct(); 
	} 

	public void correct(){
		//flash check mark 
		Image[] images = panel.GetComponentsInChildren<Image> ();

		Debug.Log("ding ding!"); 
		foreach (Image image in images) {
			if (image.gameObject.transform.parent.tag == "circle") { //if it has a parent 
				//should be child 
				image.enabled = !image.enabled; 
			} 
		} 
		//check.enabled = !check.enabled; 
	} 
}
