﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadTitle : MonoBehaviour {

	public void LoadScene() {
		SceneManager.LoadScene( "_TitleScene" );
	}
	public void LoadLose() {
		SceneManager.LoadScene( "_EndGame_Lose" );
	}

}
