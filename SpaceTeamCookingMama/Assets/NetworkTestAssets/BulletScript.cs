﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {

	void OnCollisionEnter(Collision collision)
	{
		var hit = collision.gameObject;
		var hitPlayer = hit.GetComponent<PlayerMove>();
		if (hitPlayer != null)
		{
			var combat = hit.GetComponent<Combat>();
			int health = combat.TakeDamage(10);
			if (health == 0) {
				Destroy (hit);
			}


			Destroy(gameObject);
		}
	}

}
