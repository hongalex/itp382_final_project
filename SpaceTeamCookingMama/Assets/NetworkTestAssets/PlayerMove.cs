﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerMove : NetworkBehaviour {
	public GameObject bulletPrefab;

	private Vector3 direction;
	//private Vector3 lastPosition;
	void Start () {
		direction = Vector3.right;
	}

	// Update is called once per frame
	void Update () {
		if (!isLocalPlayer) {
			return;
		}
		float x = Input.GetAxis("Horizontal") * 0.1f;
		float y = Input.GetAxis("Vertical") * 0.1f;

		transform.Translate (x, y, 0);

		if (Input.GetKeyDown(KeyCode.Space)) {
			CmdFire();
		} 
		/*Vector3 tempDirection = transform.position - lastPosition;
		direction = transform.InverseTransformDirection (tempDirection);
		lastPosition = transform.position;*/


		if (Input.GetKeyDown (KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A)) {
			CmdChangeDirection (Vector3.left);
		} else if (Input.GetKeyDown (KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D)) {
			CmdChangeDirection (Vector3.right);
		} /*else if (Input.GetKeyDown (KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)) {
			direction = transform.up;
		} else if (Input.GetKeyDown (KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S)) {
			direction = -1 * transform.up;
		}*/

	}

	[Command]
	void CmdChangeDirection(Vector3 dir) {
		direction = dir;
	}

	public override void OnStartLocalPlayer(){
		GetComponent<MeshRenderer>().material.color = Color.red;
	}

	[Command]
	void CmdFire()
	{
		// create the bullet object from the bullet prefab
		var bullet = (GameObject)Instantiate(
			bulletPrefab,
			transform.position + direction/2,
			Quaternion.identity);

		// make the bullet move away in front of the player
		bullet.GetComponent<Rigidbody>().velocity = direction*8;

		NetworkServer.Spawn(bullet);

		// make bullet disappear after 2 seconds
		Destroy(bullet, 2.0f);        
	}

}
